﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PhoneBook.Communication.Model.Communication
{
    public class DeleteCommunicationDetailRequest
    {
        public long Id { get; set; }
    }
}
