﻿using System;
using System.Collections.Generic;
using System.Text;
using PhoneBook.Shared.Model;

namespace PhoneBook.Communication.Model.Communication
{
    public class AddCommunicationDetailResponse
    {
        public long Id { get; set; }
        public CommunicationType Type { get; set; }
        public string Value { get; set; }
    }
}
