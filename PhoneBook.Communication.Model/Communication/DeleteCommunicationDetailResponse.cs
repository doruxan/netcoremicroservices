﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PhoneBook.Communication.Model.Communication
{
    public class DeleteCommunicationDetailResponse
    {
        public long Id { get; set; }
    }
}
