﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PhoneBook.Communication.Model.Communication
{
    public class ContactModel
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }

        public ICollection<CommunicationDetailModel> Communications { get; set; }
    }
}
