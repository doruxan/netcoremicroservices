﻿using System;
using System.Collections.Generic;
using System.Text;
using PhoneBook.Shared.Model;

namespace PhoneBook.Communication.Model.Communication
{
    public class AddCommunicationDetailRequest
    {
        public long ContactId { get; set; }
        public CommunicationType Type { get; set; }
        public string Value { get; set; }
    }
}
