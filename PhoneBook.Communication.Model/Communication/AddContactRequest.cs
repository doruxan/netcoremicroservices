﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PhoneBook.Communication.Model.Communication
{
    public class AddContactRequest
    {
        public string Name { get; set; }
        public string Surname { get; set; }
    }
}
