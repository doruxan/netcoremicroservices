﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PhoneBook.Communication.Model.Communication
{
    public class GetContactDetailsResponse
    {
        public ContactModel Contact { get; set; }
    }
}
