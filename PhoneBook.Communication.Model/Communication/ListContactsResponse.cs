﻿using System;
using System.Collections.Generic;
using System.Text;
using PhoneBook.Model.Core;

namespace PhoneBook.Communication.Model.Communication
{
    public class ListContactsResponse : PaginatedResponse
    {
        public List<ContactModel> Contacts { get; set; }
    }
}
