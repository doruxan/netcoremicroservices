﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using PhoneBook.Business.Core.Interfaces;
using PhoneBook.Data.Core;
using PhoneBook.Report.Data.Entities;
using PhoneBook.Report.Model;

namespace PhoneBook.Report.Business.CQRS.Commands
{
    public class CreateReport : IAsyncCommand<CreateReportResponse>
    {
        public CreateReportRequest Request { get; set; }
        public bool Result { get; set; }
        public CreateReportResponse ReturnValue { get; set; }
    }

    public class CreateReportHandler : IAsyncCommandHandler<CreateReport, CreateReportResponse>
    {
        private readonly IRepository<CommunicationReportEntity> repository;
        private readonly IUnitOfWork unitOfWork;
        private readonly IMapper mapper;

        public CreateReportHandler(
            IRepository<CommunicationReportEntity> repository,
            IUnitOfWork unitOfWork,
            IMapper mapper)
        {
            this.repository = repository;
            this.unitOfWork = unitOfWork;
            this.mapper = mapper;
        }

        public void Handle(CreateReport command)
        {
            HandleAsync(command).GetAwaiter().GetResult();
        }

        public async Task HandleAsync(CreateReport command)
        {
            var entity = mapper.Map<CommunicationReportEntity>(command.Request);

            await repository.AddAsync(entity);

            await unitOfWork.CommitAsync();

            command.ReturnValue = mapper.Map<CreateReportResponse>(entity);
        }
    }
}
