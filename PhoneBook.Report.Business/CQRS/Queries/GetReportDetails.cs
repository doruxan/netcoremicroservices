﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using PhoneBook.Business.Core.Interfaces;
using PhoneBook.Data.Core;
using PhoneBook.Report.Data.Entities;
using PhoneBook.Report.Model;

namespace PhoneBook.Report.Business.CQRS.Queries
{

    public class GetReportDetails : IAsyncQuery<GetReportDetailsResponse>
    {
        public GetReportDetailsRequest Request { get; set; }
    }

    public class GetReportDetailsHandler : IAsyncQueryExecutor<GetReportDetails, GetReportDetailsResponse>
    {
        private readonly IRepository<CommunicationReportEntity> repository;
        private readonly IRepository<CommunicationReportDetailEntity> detailRepository;
        private readonly IMapper mapper;

        public GetReportDetailsHandler(
            IRepository<CommunicationReportEntity> repository,
            IRepository<CommunicationReportDetailEntity> detailRepository,
            IMapper mapper)
        {
            this.repository = repository;
            this.detailRepository = detailRepository;
            this.mapper = mapper;
        }

        public GetReportDetailsResponse Execute(GetReportDetails query)
        {
            return Task.Run(async () => { return await ExecuteAsync(query).ConfigureAwait(false); }).Result;
        }

        public async Task<GetReportDetailsResponse> ExecuteAsync(GetReportDetails query)
        {
            var report = repository
                                .GetQueryable()
                                .FirstOrDefault(x => x.Id == query.Request.Id);

            var detail = detailRepository
                                .GetQueryable()
                                .FirstOrDefault(x => x.ReportId == query.Request.Id);

            report.Detail = detail;

            return new GetReportDetailsResponse
            {
                Report = mapper.Map<CommunicationReportModel>(report)
            };
        }
    }
}
