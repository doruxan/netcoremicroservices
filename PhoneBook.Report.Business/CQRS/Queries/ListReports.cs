﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using PhoneBook.Business.Core.Interfaces;
using PhoneBook.Data.Core;
using PhoneBook.Report.Data.Entities;
using PhoneBook.Report.Model;

namespace PhoneBook.Report.Business.CQRS.Queries
{

    public class ListReports : IAsyncQuery<ListReportsResponse>
    {
        public ListReportsRequest Request { get; set; }
    }

    public class ListReportsHandler : IAsyncQueryExecutor<ListReports, ListReportsResponse>
    {
        private readonly IRepository<CommunicationReportEntity> repository;
        private readonly IMapper mapper;

        public ListReportsHandler(
            IRepository<CommunicationReportEntity> repository,
            IMapper mapper)
        {
            this.repository = repository;
            this.mapper = mapper;
        }

        public ListReportsResponse Execute(ListReports query)
        {
            return Task.Run(async () => { return await ExecuteAsync(query).ConfigureAwait(false); }).Result;
        }

        public async Task<ListReportsResponse> ExecuteAsync(ListReports query)
        {
            var reports =  repository
                                .GetQueryable()
                                .OrderBy(x => x.UnixTimestamp)
                                .Skip((query.Request.Page - 1) * query.Request.PageSize)
                                .Take(query.Request.PageSize)
                                .Select(x => new CommunicationReportModel { Id = x.Id, RequestDate = x.RequestDate, Status = x.Status, Location = x.Location })
                                .ToList();

            var totalCount = repository.GetQueryable().Count();
            decimal maxPages = totalCount / query.Request.PageSize;
            return new ListReportsResponse
            {
                Page = query.Request.Page,
                PageSize = query.Request.PageSize,
                TotalCount = totalCount,
                MaxPages = (int)Math.Ceiling(maxPages),
                Reports = reports
            };
        }
    }
}
