﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using PhoneBook.Business.Core.Interfaces;
using PhoneBook.Data.Core;
using PhoneBook.Report.Data.Entities;
using PhoneBook.Shared.Business.Events;
using PhoneBook.Shared.Model;

namespace PhoneBook.Report.Business.CQRS.Events
{
    
    public class CommunicationDetailAddedHandler : IAsyncEventHandler<CommunicationDetailAdded>
    {
        private readonly IRepository<CustomerDetailEntity> repository;
        private readonly IUnitOfWork unitOfWork;
        private readonly IMapper mapper;

        public CommunicationDetailAddedHandler(
            IRepository<CustomerDetailEntity> repository,
            IUnitOfWork unitOfWork,
            IMapper mapper)
        {
            this.repository = repository;
            this.unitOfWork = unitOfWork;
            this.mapper = mapper;
        }
        public void Handle(CommunicationDetailAdded @event)
        {
            Task.Run(async () => { await HandleAsync(@event).ConfigureAwait(false); }).Wait();
        }

        public async Task HandleAsync(CommunicationDetailAdded @event)
        {
            var entity = repository.GetQueryable().Where(x => x.CustomerId == @event.ContactId).FirstOrDefault();

            if(entity != null)
            {
                if (@event.Type == CommunicationType.Address)
                    entity.Location = @event.Value;

                if (@event.Type == CommunicationType.Phone)
                    entity.PhoneCount = entity.PhoneCount + 1;

                await repository.UpdateAsync(entity);
                await unitOfWork.CommitAsync();
                return;
            }

            entity = new CustomerDetailEntity();
            entity.CustomerId = @event.ContactId;
            if (@event.Type == CommunicationType.Address)
                entity.Location = @event.Value;

            if (@event.Type == CommunicationType.Phone)
                entity.PhoneCount = entity.PhoneCount + 1;

            await repository.AddAsync(entity);
            await unitOfWork.CommitAsync();
        }
    }
}
