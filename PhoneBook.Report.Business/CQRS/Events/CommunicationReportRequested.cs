﻿using System;
using System.Collections.Generic;
using System.Text;
using PhoneBook.Business.Core.Interfaces;

namespace PhoneBook.Report.Business.CQRS.Events
{
    public class CommunicationReportRequested: IEvent
    {
        public string ReportId { get; set; }
        public string Location { get; set; }
    }
}
