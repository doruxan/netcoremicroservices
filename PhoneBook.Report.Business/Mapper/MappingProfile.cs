﻿using System;
using System.Collections.Generic;
using System.Text;
using AutoMapper;
using PhoneBook.Report.Data.Entities;
using PhoneBook.Report.Model;

namespace PhoneBook.Report.Business.Mapper
{
    public class MappingProfile: Profile
    {
        public MappingProfile()
        {
            CreateMap<CommunicationReportEntity, CommunicationReportModel>();
            CreateMap<CommunicationReportDetailEntity, CommunicationReportDetailModel>();

            CreateMap<CreateReportRequest, CommunicationReportEntity>();
            CreateMap<CommunicationReportEntity, CreateReportResponse>();
        }
    }
}
