﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Confluent.Kafka;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Newtonsoft.Json;
using PhoneBook.Business.Core;
using PhoneBook.Data.Core;
using PhoneBook.Report.Business.CQRS.Events;
using PhoneBook.Report.Data.Entities;

namespace PhoneBook.Report.Business.Services
{
    public class ReportService : IHostedService
    {
        private readonly string topic;
        private readonly IConsumer<Ignore, string> kafkaConsumer;
        private readonly IServiceProvider serviceProvider;

        public ReportService(
                IServiceProvider serviceProvider
            )
        {
            var consumerConfig = new ConsumerConfig
            {
                BootstrapServers = "localhost:9092",
                GroupId = "foo",
                //AutoOffsetReset = AutoOffsetReset.Earliest
            };
            //this.topic = config.GetSection("Kafka")["Topic"];            
            this.topic = "report";
            this.kafkaConsumer = new ConsumerBuilder<Ignore, string>(consumerConfig).Build();
           
            this.serviceProvider = serviceProvider;
        }
        public async Task StartAsync(CancellationToken cancellationToken)
        {
            new Thread(async () => await Subscribe(cancellationToken)).Start();
        }

        public async Task Subscribe(CancellationToken cancellationToken)
        {
            kafkaConsumer.Subscribe(this.topic);

            // from kafka examples
            while (!cancellationToken.IsCancellationRequested)
            {
                try
                {
                    var cr = this.kafkaConsumer.Consume(cancellationToken);

                    var message = JsonConvert.DeserializeObject<QueueMessage>(cr.Message.Value);
                    var @event = JsonConvert.DeserializeObject<CommunicationReportRequested>(message.Payload);

                    var factory = serviceProvider.GetService<IServiceScopeFactory>();

                    using (var scope = factory.CreateScope())
                    {
                        var communicaitonReportRepository = scope.ServiceProvider.GetService<IRepository<CommunicationReportEntity>>();
                        var communicaitonReportDetailRepository = scope.ServiceProvider.GetService<IRepository<CommunicationReportDetailEntity>>();
                        var customerDetailRepository = scope.ServiceProvider.GetService<IRepository<CustomerDetailEntity>>();
                        var unitOfWork = scope.ServiceProvider.GetService<IUnitOfWork>();

                        var locationData = customerDetailRepository.GetQueryable().Where(x => x.Location == @event.Location).ToList();
                        var contactCount = locationData.Count();
                        var phoneCount = locationData.Sum(x => x.PhoneCount);

                        var entity = new CommunicationReportDetailEntity
                        {
                            Location = @event.Location,
                            PhoneCount = phoneCount,
                            ContactCount = contactCount,
                            ReportId = @event.ReportId
                        };

                        var reportEntity = await communicaitonReportRepository.GetByIdAsync(@event.ReportId);
                        entity.ReportId = reportEntity.Id;

                        await communicaitonReportDetailRepository.AddAsync(entity);

                        reportEntity.DetailId = entity.Id;
                        reportEntity.Status = Model.ReportStatus.Done;

                        await communicaitonReportRepository.UpdateAsync(reportEntity);
                        await unitOfWork.CommitAsync();
                    }

                    Console.WriteLine($"{cr.Message.Key}: {cr.Message.Value}ms");
                }
                catch (OperationCanceledException)
                {
                    break;
                }
                catch (ConsumeException e)
                {
                    // Consumer errors should generally be ignored (or logged) unless fatal.
                    Console.WriteLine($"Consume error: {e.Error.Reason}");

                    if (e.Error.IsFatal)
                    {
                        // https://github.com/edenhill/librdkafka/blob/master/INTRODUCTION.md#fatal-consumer-errors
                        break;
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine($"Unexpected error: {e}");
                    break;
                }
            }
        }

        public async Task StopAsync(CancellationToken cancellationToken)
        {
            this.kafkaConsumer.Close(); // Commit offsets and leave the group cleanly.
            this.kafkaConsumer.Dispose();
        }
    }
}
