using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.OpenApi.Models;
using PhoneBook.Business.Core;
using PhoneBook.Business.Core.Interfaces;
using PhoneBook.Communication.Business.CQRS;
using PhoneBook.Communication.Business.Mapper;
using PhoneBook.Communication.Data;
using PhoneBook.Data.Core;
using PhoneBook.Data.Core.Relational;

namespace PhoneBook.Communication.API
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {

            services.AddControllers();
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "PhoneBook.Communication.API", Version = "v1" });
            });

            services.AddDbContext<CommunicationDbContext>(options =>
            {
                options.UseNpgsql(Configuration.GetConnectionString("SQLConnection"));
            });


            services.AddAutoMapper(typeof(MappingProfile));
            services.AddScoped<DbContext, CommunicationDbContext>();
            services.AddScoped<IUnitOfWork, RelationalUnitOfWork>();
            services.AddScoped(typeof(IRepository<>), typeof(BaseRelationalRepository<>));
            services.AddSingleton<IQueueClientHandle, QueueClientHandle>();
            services.AddSingleton(typeof(IQueueMessageProducer<,>), typeof(QueueMessageProducer<,>));
            services.AddSingleton<IEventBus, EventBus>();

            BusinessBootstrapper.RegisterDependencies(services);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "PhoneBook.Communication.API v1"));
            }

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
