﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using PhoneBook.Business.Core;
using PhoneBook.Business.Core.Interfaces;
using PhoneBook.Communication.Business.CQRS.Commands;
using PhoneBook.Communication.Business.CQRS.Queries;
using PhoneBook.Communication.Model.Communication;
using PhoneBook.Shared.Business.Events;

namespace PhoneBook.Communication.API.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    [Produces("application/json")]
    public class CommunicationController : ControllerBase
    {
        private readonly ILogger<CommunicationController> _logger;
        private readonly ICommandBus commandBus;
        private readonly IQueryProcessor queryProcessor;
        private readonly IEventBus eventBus;

        public CommunicationController(
            ILogger<CommunicationController> logger,
            ICommandBus commandBus,
            IQueryProcessor queryProcessor,
            IEventBus eventBus
            )
        {
            _logger = logger;
            this.commandBus = commandBus;
            this.queryProcessor = queryProcessor;
            this.eventBus = eventBus;
        }

        [HttpPost]
        [Route("Contact")]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> AddContact(AddContactRequest request)
        {
            var command = new AddContact { Request = request };
            await commandBus.SendAsync<AddContact, AddContactResponse>(command);
            return StatusCode(201, command.ReturnValue);
        }

        [HttpDelete]
        [Route("Contact")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> DeleteContact(DeleteContactRequest request)
        {
            var command = new DeleteContact { Request = request };
            await commandBus.SendAsync<DeleteContact, DeleteContactResponse>(command);
            return StatusCode(200, command.ReturnValue);
        }

        /// <summary>
        /// post because function is not idempotent
        /// </summary>
        [HttpPost]
        [Route("Contact/Communicaiton")]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> AddCommunicaitonToContact(AddCommunicationDetailRequest request)
        {
            var command = new AddCommunicationDetail { Request = request };
            await commandBus.SendAsync<AddCommunicationDetail, AddCommunicationDetailResponse>(command);
            var @event = new CommunicationDetailAdded { ContactId = request.ContactId, Type = command.ReturnValue.Type, Value = command.ReturnValue.Value };
            await eventBus.PublishAsync("demo", @event);
            return StatusCode(201, command.ReturnValue);
        }

        [HttpDelete]
        [Route("Contact/Communicaiton")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> DeleteCommunicaitonFromContact(DeleteCommunicationDetailRequest request)
        {
            var command = new DeleteCommunicationDetail { Request = request };
            await commandBus.SendAsync<DeleteCommunicationDetail, DeleteCommunicationDetailResponse>(command);
            return StatusCode(200, command.ReturnValue);
        }

        [HttpGet]
        [Route("Contact/List")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> ListContacts(int page = 1, int pageSize = 20)
        {
            var request = new ListContactsRequest { Page = page, PageSize = pageSize };
            var query = new ListContacts { Request = request };
            var result = await queryProcessor.ProcessAsync(query);
            return StatusCode(200, result);
        }

        [HttpGet]
        [Route("Contact/Detail")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> GetContactDetails(long contactId)
        {
            var request = new GetContactDetailsRequest { Id = contactId };
            var query = new GetContactDetails { Request = request };
            var result = await queryProcessor.ProcessAsync(query);
            return StatusCode(200, result);
        }

    }
}
