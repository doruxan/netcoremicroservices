using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using PhoneBook.Communication.Business.CQRS.Commands;
using PhoneBook.Communication.Business.Mapper;
using PhoneBook.Communication.Data;
using PhoneBook.Communication.Data.Entites;
using PhoneBook.Communication.Model.Communication;
using PhoneBook.Data.Core.Relational;
using PhoneBook.Shared.Model;
using Xunit;

namespace PhoneBook.Communication.Test
{
    [Collection("Sequential")]
    public class CommandTests
    {
        [Fact]
        public async System.Threading.Tasks.Task AddContactAsync()
        {
            //var options = new DbContextOptionsBuilder<CommunicationDbContext>()
            //    .UseInMemoryDatabase(databaseName: GenerateDbName())
            //    .Options;

            using (var context = new CommunicationDbContext(CreateNewContextOptions()))
            {
                var command = new AddContact
                {
                    Request = new AddContactRequest
                    {
                        Name = "John",
                        Surname = "Wick"
                    }
                };
                var repository = new BaseRelationalRepository<ContactEntity>(context);
                var unitOfWork = new RelationalUnitOfWork(context);
                var mockMapper = new MapperConfiguration(cfg =>
                {
                    cfg.AddProfile(new MappingProfile());
                });
                var mapper = mockMapper.CreateMapper();
                var commandHandler = new AddContactHandler(repository, unitOfWork, mapper);

                await commandHandler.HandleAsync(command);

                Assert.Equal(1, command.ReturnValue.Id);

                var addedContact = context.Contacts.FirstOrDefault();

                Assert.Equal("John", addedContact.Name);

                context.Entry(addedContact).State = EntityState.Detached;
                context.SaveChanges();
            }
        }


        [Fact]
        public async System.Threading.Tasks.Task DeleteContactAsync()
        {
            //var options = new DbContextOptionsBuilder<CommunicationDbContext>()
            //    .UseInMemoryDatabase(databaseName: GenerateDbName())
            //    .Options;

            using (var context = new CommunicationDbContext(CreateNewContextOptions()))
            {
                var contact = new ContactEntity
                {
                    Name = "John",
                    Surname = "Wick",
                    //CreatedDate = DateTimeOffset.UtcNow,            
                    Communications = new List<CommunicationDetailEntity> { }
                };
                var detail = new CommunicationDetailEntity
                {
                    Type = CommunicationType.Phone,
                    Value = "00905544555445"
                };
                contact.Communications.Add(detail);
                context.Contacts.Add(contact);
                context.SaveChanges();

                var command = new DeleteContact
                {
                    Request = new DeleteContactRequest
                    {
                        Id = 1
                    }
                };

                var repository = new BaseRelationalRepository<ContactEntity>(context);
                var unitOfWork = new RelationalUnitOfWork(context);
                var mockMapper = new MapperConfiguration(cfg =>
                {
                    cfg.AddProfile(new MappingProfile());
                });
                var mapper = mockMapper.CreateMapper();

                var commandHandler = new DeleteContactHandler(repository, unitOfWork, mapper);

                await commandHandler.HandleAsync(command);

                Assert.Equal(1, command.ReturnValue.Id);

                var contacts = context.Contacts.ToList();

                Assert.Empty(contacts);
            }
        }



        [Fact]
        public async System.Threading.Tasks.Task DeleteCommunicationDetailAsync()
        {
            //var options = new DbContextOptionsBuilder<CommunicationDbContext>()
            //    .UseInMemoryDatabase(databaseName: GenerateDbName())
            //    .Options;

            using (var context = new CommunicationDbContext(CreateNewContextOptions()))
            {
                var contact = new ContactEntity
                {
                    Name = "John",
                    Surname = "Wick",
                    //CreatedDate = DateTimeOffset.UtcNow,            
                    Communications = new List<CommunicationDetailEntity> { }
                };
                var detail = new CommunicationDetailEntity
                {
                    Type = CommunicationType.Phone,
                    Value = "00905544555445"
                };
                contact.Communications.Add(detail);
                context.Contacts.Add(contact);
                context.SaveChanges();

                var command = new DeleteCommunicationDetail
                {
                    Request = new DeleteCommunicationDetailRequest
                    {
                        Id = 1
                    }
                };
                var repository = new BaseRelationalRepository<CommunicationDetailEntity>(context);
                var unitOfWork = new RelationalUnitOfWork(context);
                var mockMapper = new MapperConfiguration(cfg =>
                {
                    cfg.AddProfile(new MappingProfile());
                });

                var mapper = mockMapper.CreateMapper();
                var commandHandler = new DeleteCommunicationDataHandler(repository, unitOfWork, mapper);

                await commandHandler.HandleAsync(command);

                Assert.Equal(1, command.ReturnValue.Id);

                var updatedContact = context.Contacts.FirstOrDefault();

                Assert.Empty(updatedContact.Communications);

                context.Entry(updatedContact).State = EntityState.Detached;
                context.SaveChanges();

            }
        }

        [Fact]
        public async System.Threading.Tasks.Task AddCommunicationDetailAsync()
        {
            //var options = new DbContextOptionsBuilder<CommunicationDbContext>()
            //    .UseInMemoryDatabase(databaseName: GenerateDbName())
            //    .Options;

            using (var context = new CommunicationDbContext(CreateNewContextOptions()))
            {
                var command = new AddCommunicationDetail
                {
                    Request = new AddCommunicationDetailRequest
                    {
                        ContactId = 1,
                        Type = CommunicationType.Phone,
                        Value = "00905544555445"
                    }
                };
                var repository = new BaseRelationalRepository<CommunicationDetailEntity>(context);
                var unitOfWork = new RelationalUnitOfWork(context);
                var mockMapper = new MapperConfiguration(cfg =>
                {
                    cfg.AddProfile(new MappingProfile());
                });
                var mapper = mockMapper.CreateMapper();
                var commandHandler = new AddCommunicationDataHandler(repository, unitOfWork, mapper);

                await commandHandler.HandleAsync(command);

                Assert.Equal(1, command.ReturnValue.Id);

                var commDetail = context.CommunicationDetails.FirstOrDefault();
                var contact = context.Contacts.FirstOrDefault();
                context.Entry(commDetail).State = EntityState.Detached;
                context.Entry(contact).State = EntityState.Detached;
                context.SaveChanges();

            }
        }

        private static DbContextOptions<CommunicationDbContext> CreateNewContextOptions()
        {
            // Create a fresh service provider, and therefore a fresh 
            // InMemory database instance.
            var serviceProvider = new ServiceCollection()
                .AddEntityFrameworkInMemoryDatabase()
                .BuildServiceProvider();

            // Create a new options instance telling the context to use an
            // InMemory database and the new service provider.
            var builder = new DbContextOptionsBuilder<CommunicationDbContext>();
            builder.UseInMemoryDatabase("commDb")
                   .UseQueryTrackingBehavior(QueryTrackingBehavior.NoTracking)
                   .EnableSensitiveDataLogging()
                   .UseInternalServiceProvider(serviceProvider);

            return builder.Options;
        }
    }
}
