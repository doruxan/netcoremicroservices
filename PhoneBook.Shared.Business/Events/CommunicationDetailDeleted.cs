﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using PhoneBook.Business.Core.Interfaces;
using PhoneBook.Shared.Model;

namespace PhoneBook.Shared.Business.Events
{
    public class CommunicationDetailDeleted : IEvent
    {
        public long ContactId { get; set; }
        public CommunicationType Type { get; set; }
    }

}
