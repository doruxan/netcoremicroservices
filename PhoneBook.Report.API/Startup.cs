using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.OpenApi.Models;
using PhoneBook.Business.Core;
using PhoneBook.Business.Core.Interfaces;
using PhoneBook.Data.Core;
using PhoneBook.Data.Core.NonRelational;
using PhoneBook.Report.Business.CQRS;
using PhoneBook.Report.Business.Mapper;
using PhoneBook.Report.Business.Services;

namespace PhoneBook.Report.API
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {

            services.AddControllers();
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "PhoneBook.Report.API", Version = "v1" });
            });

            services.AddAutoMapper(typeof(MappingProfile));
            services.AddScoped<IMongoContext, MongoContext>();
            services.AddScoped<IUnitOfWork, MongoUnitOfWork>();
            services.AddScoped(typeof(IRepository<>), typeof(BaseNonRelationalRepository<>));
            services.AddSingleton<IEventProcessor, EventProcessor>();
            services.AddHostedService<QueueMessageListener>();
            services.AddSingleton<IQueueClientHandle, QueueClientHandle>();
            services.AddSingleton(typeof(IQueueMessageProducer<,>), typeof(QueueMessageProducer<,>));
            services.AddSingleton<IEventBus, EventBus>();
            services.AddHostedService<ReportService>();

            BusinessBootstrapper.RegisterDependencies(services);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "PhoneBook.Report.API v1"));
            }

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
