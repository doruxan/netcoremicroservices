﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using PhoneBook.Business.Core.Interfaces;
using PhoneBook.Report.Business.CQRS.Commands;
using PhoneBook.Report.Business.CQRS.Events;
using PhoneBook.Report.Business.CQRS.Queries;
using PhoneBook.Report.Model;

namespace PhoneBook.Report.API.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    [Produces("application/json")]
    public class ReportController : ControllerBase
    {
        private readonly ILogger<ReportController> _logger;
        private readonly ICommandBus commandBus;
        private readonly IQueryProcessor queryProcessor;
        private readonly IEventBus eventBus;

        public ReportController(
                   ILogger<ReportController> logger,
                   ICommandBus commandBus,
                   IQueryProcessor queryProcessor,
                   IEventBus eventBus)
        {
            _logger = logger;
            this.commandBus = commandBus;
            this.queryProcessor = queryProcessor;
            this.eventBus = eventBus;
        }

        [HttpGet]
        [Route("List")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> ListReports(int page = 1, int pageSize = 20)
        {
            var request = new ListReportsRequest { Page = page, PageSize = pageSize };
            var query = new ListReports { Request = request };
            var result = await queryProcessor.ProcessAsync(query);
            return StatusCode(200, result);
        }

        [HttpGet]
        [Route("Detail")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> GetReportDetails(string id)
        {
            var request = new GetReportDetailsRequest { Id = id };
            var query = new GetReportDetails { Request = request };
            var result = await queryProcessor.ProcessAsync(query);
            return StatusCode(200, result);
        }

        [HttpPost]
        [Route("Request")]
        [ProducesResponseType(StatusCodes.Status202Accepted)]
        [ProducesResponseType(StatusCodes.Status406NotAcceptable)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> RequestReport(string location)
        {
            var request = new CreateReportRequest { Location = location };
            var command = new CreateReport { Request = request };
            await commandBus.SendAsync<CreateReport, CreateReportResponse>(command);
            var @event = new CommunicationReportRequested { ReportId = command.ReturnValue.Id, Location = command.ReturnValue.Location };
            await eventBus.PublishAsync("report", @event);
            return StatusCode(202, command.ReturnValue);
        }
    }
}
