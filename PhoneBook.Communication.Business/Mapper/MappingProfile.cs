﻿using System;
using System.Collections.Generic;
using System.Text;
using AutoMapper;
using PhoneBook.Communication.Data.Entites;
using PhoneBook.Communication.Model.Communication;

namespace PhoneBook.Communication.Business.Mapper
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<AddContactRequest, ContactEntity>();
            CreateMap<ContactEntity, AddContactResponse>();

            CreateMap<DeleteContactRequest, ContactEntity>();
            CreateMap<ContactEntity, DeleteContactResponse>();

            CreateMap<AddCommunicationDetailRequest, CommunicationDetailEntity>();
            CreateMap<CommunicationDetailEntity, AddCommunicationDetailResponse>();


            CreateMap<DeleteCommunicationDetailRequest, CommunicationDetailEntity>();
            CreateMap<CommunicationDetailEntity, DeleteCommunicationDetailResponse>();


            CreateMap<CommunicationDetailEntity, CommunicationDetailModel>().ReverseMap();
            CreateMap<ContactEntity, ContactModel>().ReverseMap();
        }
    }
}
