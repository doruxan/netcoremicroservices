﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using PhoneBook.Business.Core.Interfaces;
using PhoneBook.Communication.Data.Entites;
using PhoneBook.Communication.Model.Communication;
using PhoneBook.Data.Core;

namespace PhoneBook.Communication.Business.CQRS.Queries
{
    public class GetContactDetails : IAsyncQuery<GetContactDetailsResponse>
    {
        public GetContactDetailsRequest Request { get; set; }
    }

    public class GetContactDetailsHandler : IAsyncQueryExecutor<GetContactDetails, GetContactDetailsResponse>
    {
        private readonly IRepository<ContactEntity> repository;
        private readonly IMapper mapper;

        public GetContactDetailsHandler(
            IRepository<ContactEntity> repository,
            IMapper mapper)
        {
            this.repository = repository;
            this.mapper = mapper;
        }

        public GetContactDetailsResponse Execute(GetContactDetails query)
        {
            return Task.Run(async () => { return await ExecuteAsync(query).ConfigureAwait(false); }).Result;
        }

        public async Task<GetContactDetailsResponse> ExecuteAsync(GetContactDetails query)
        {
            var contact = await repository
                                .GetQueryable()
                                .Include(x => x.Communications)
                                .FirstOrDefaultAsync(x => x.Id == query.Request.Id);

            return new GetContactDetailsResponse
            {
                Contact = mapper.Map<ContactModel>(contact)
            };
        }
    }
}
