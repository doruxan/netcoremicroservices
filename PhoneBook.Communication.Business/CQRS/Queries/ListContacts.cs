﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using PhoneBook.Business.Core.Interfaces;
using PhoneBook.Communication.Data.Entites;
using PhoneBook.Communication.Model.Communication;
using PhoneBook.Data.Core;

namespace PhoneBook.Communication.Business.CQRS.Queries
{
    public class ListContacts : IAsyncQuery<ListContactsResponse>
    {
        public ListContactsRequest Request { get; set; }
    }

    public class ListContactsHandler : IAsyncQueryExecutor<ListContacts, ListContactsResponse>
    {
        private readonly IRepository<ContactEntity> repository;
        private readonly IMapper mapper;

        public ListContactsHandler(
            IRepository<ContactEntity> repository,
            IMapper mapper)
        {
            this.repository = repository;
            this.mapper = mapper;
        }

        public ListContactsResponse Execute(ListContacts query)
        {
            return Task.Run(async () => { return await ExecuteAsync(query).ConfigureAwait(false); }).Result;
        }

        public async Task<ListContactsResponse> ExecuteAsync(ListContacts query)
        {
            var contacts = await repository
                                .GetQueryable()
                                .OrderBy(x => x.Id)
                                .Skip((query.Request.Page - 1) * query.Request.PageSize)
                                .Take(query.Request.PageSize)
                                .Select(x => new ContactModel { Id = x.Id, Name = x.Name, Surname = x.Surname})
                                .ToListAsync();

            var totalCount = await repository.GetQueryable().CountAsync();
            decimal maxPages = totalCount / query.Request.PageSize;
            return new ListContactsResponse
            {
                Page = query.Request.Page,
                PageSize = query.Request.PageSize,
                TotalCount = totalCount,
                MaxPages = (int)Math.Ceiling(maxPages),
                Contacts = contacts
            };
        }
    }
}
