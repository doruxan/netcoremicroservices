﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using PhoneBook.Business.Core.Interfaces;
using PhoneBook.Communication.Data.Entites;
using PhoneBook.Communication.Model.Communication;
using PhoneBook.Data.Core;

namespace PhoneBook.Communication.Business.CQRS.Commands
{
    public class AddCommunicationDetail : IAsyncCommand<AddCommunicationDetailResponse>
    {
        public AddCommunicationDetailRequest Request { get; set; }
        public bool Result { get; set; }
        public AddCommunicationDetailResponse ReturnValue { get; set; }
    }

    public class AddCommunicationDataHandler : IAsyncCommandHandler<AddCommunicationDetail, AddCommunicationDetailResponse>
    {
        private readonly IRepository<CommunicationDetailEntity> repository;
        private readonly IUnitOfWork unitOfWork;
        private readonly IMapper mapper;

        public AddCommunicationDataHandler(
            IRepository<CommunicationDetailEntity> repository,
            IUnitOfWork unitOfWork,
            IMapper mapper)
        {
            this.repository = repository;
            this.unitOfWork = unitOfWork;
            this.mapper = mapper;
        }

        public void Handle(AddCommunicationDetail command)
        {
            HandleAsync(command).GetAwaiter().GetResult();
        }

        public async Task HandleAsync(AddCommunicationDetail command)
        {
            var entity = mapper.Map<CommunicationDetailEntity>(command.Request);
            await repository.AddAsync(entity);

            await unitOfWork.CommitAsync();

            command.ReturnValue = mapper.Map<AddCommunicationDetailResponse>(entity);
        }
    }
}
