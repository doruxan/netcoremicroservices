﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using PhoneBook.Business.Core.Interfaces;
using PhoneBook.Communication.Business.CQRS.Queries;
using PhoneBook.Communication.Data.Entites;
using PhoneBook.Communication.Model.Communication;
using PhoneBook.Data.Core;

namespace PhoneBook.Communication.Business.CQRS.Commands
{
    public class AddContact : IAsyncCommand<AddContactResponse>
    {
        public AddContactRequest Request { get; set; }
        public bool Result { get; set; }
        public AddContactResponse ReturnValue { get ; set; }
    }

    public class AddContactHandler : IAsyncCommandHandler<AddContact, AddContactResponse>
    {
        private readonly IRepository<ContactEntity> repository;
        private readonly IUnitOfWork unitOfWork;
        private readonly IMapper mapper;

        public AddContactHandler(
            IRepository<ContactEntity> repository, 
            IUnitOfWork unitOfWork,
            IMapper mapper)
        {
            this.repository = repository;
            this.unitOfWork = unitOfWork;
            this.mapper = mapper;
        }

        public void Handle(AddContact command)
        {
            HandleAsync(command).GetAwaiter().GetResult();
        }

        public async Task HandleAsync(AddContact command)
        {
            var entity = mapper.Map<ContactEntity>(command.Request);
            await repository.AddAsync(entity);

            await unitOfWork.CommitAsync();

            command.ReturnValue = mapper.Map<AddContactResponse>(entity);
        }
    }
}
