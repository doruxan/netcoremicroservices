﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using PhoneBook.Business.Core.Interfaces;
using PhoneBook.Communication.Data.Entites;
using PhoneBook.Communication.Model.Communication;
using PhoneBook.Data.Core;

namespace PhoneBook.Communication.Business.CQRS.Commands
{
    public class DeleteCommunicationDetail : IAsyncCommand<DeleteCommunicationDetailResponse>
    {
        public DeleteCommunicationDetailRequest Request { get; set; }
        public bool Result { get; set; }
        public DeleteCommunicationDetailResponse ReturnValue { get; set; }
    }

    public class DeleteCommunicationDataHandler : IAsyncCommandHandler<DeleteCommunicationDetail, DeleteCommunicationDetailResponse>
    {
        private readonly IRepository<CommunicationDetailEntity> repository;
        private readonly IUnitOfWork unitOfWork;
        private readonly IMapper mapper;

        public DeleteCommunicationDataHandler(
            IRepository<CommunicationDetailEntity> repository,
            IUnitOfWork unitOfWork,
            IMapper mapper)
        {
            this.repository = repository;
            this.unitOfWork = unitOfWork;
            this.mapper = mapper;
        }

        public void Handle(DeleteCommunicationDetail command)
        {
            HandleAsync(command).GetAwaiter().GetResult();
        }

        public async Task HandleAsync(DeleteCommunicationDetail command)
        {
            var entity = mapper.Map<CommunicationDetailEntity>(command.Request);
            await repository.DeleteAsync(entity);

            await unitOfWork.CommitAsync();

            command.ReturnValue = mapper.Map<DeleteCommunicationDetailResponse>(entity);
        }
    }
}
