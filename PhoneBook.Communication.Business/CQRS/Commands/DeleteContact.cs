﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using PhoneBook.Business.Core.Interfaces;
using PhoneBook.Communication.Data.Entites;
using PhoneBook.Communication.Model.Communication;
using PhoneBook.Data.Core;

namespace PhoneBook.Communication.Business.CQRS.Commands
{
    public class DeleteContact : IAsyncCommand<DeleteContactResponse>
    {
        public DeleteContactRequest Request { get; set; }
        public bool Result { get; set; }
        public DeleteContactResponse ReturnValue { get; set; }
    }

    public class DeleteContactHandler : IAsyncCommandHandler<DeleteContact, DeleteContactResponse>
    {
        private readonly IRepository<ContactEntity> repository;
        private readonly IUnitOfWork unitOfWork;
        private readonly IMapper mapper;

        public DeleteContactHandler(
            IRepository<ContactEntity> repository,
            IUnitOfWork unitOfWork,
            IMapper mapper)
        {
            this.repository = repository;
            this.unitOfWork = unitOfWork;
            this.mapper = mapper;
        }

        public void Handle(DeleteContact command)
        {
            HandleAsync(command).GetAwaiter().GetResult();
        }

        public async Task HandleAsync(DeleteContact command)
        {
            var entity = mapper.Map<ContactEntity>(command.Request);
            await repository.DeleteAsync(entity);

            await unitOfWork.CommitAsync();

            command.ReturnValue = mapper.Map<DeleteContactResponse>(entity);
        }
    }
}
