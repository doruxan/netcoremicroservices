﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PhoneBook.Report.Model
{
    public class CreateReportResponse
    {
        public ReportStatus Status { get; set; } = ReportStatus.Pending;
        public DateTimeOffset RequestDate { get; set; }
        public string Location { get; set; }
        public string Id { get; set; }
    }
}
