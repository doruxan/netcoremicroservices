﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PhoneBook.Report.Model
{
    public class CommunicationReportDetailModel
    {
        public string Location { get; set; }
        public long PhoneCount { get; set; }
        public long ContactCount { get; set; }
    }
}
