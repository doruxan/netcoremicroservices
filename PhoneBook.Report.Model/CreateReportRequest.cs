﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PhoneBook.Report.Model
{
    public class CreateReportRequest
    {
        public DateTimeOffset RequestDate { get; set; } = DateTimeOffset.UtcNow;
        public string Location { get; set; }
    }
}
