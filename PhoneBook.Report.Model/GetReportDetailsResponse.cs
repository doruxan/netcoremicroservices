﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PhoneBook.Report.Model
{
    public class GetReportDetailsResponse
    {
        public CommunicationReportModel Report { get; set; }
    }
}
