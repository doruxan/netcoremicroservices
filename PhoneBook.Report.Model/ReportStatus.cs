﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace PhoneBook.Report.Model
{
    [JsonConverter(typeof(StringEnumConverter))]
    public enum ReportStatus
    {
        [EnumMember(Value = "Pending")]
        Pending,

        [EnumMember(Value = "Done")]
        Done
    }
}
