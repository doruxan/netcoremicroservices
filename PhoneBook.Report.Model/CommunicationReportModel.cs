﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PhoneBook.Report.Model
{
    public class CommunicationReportModel
    {
        public string Id { get; set; }
        public DateTimeOffset RequestDate { get; set; }
        public ReportStatus Status { get; set; }

        public string Location { get; set; }
        public CommunicationReportDetailModel Detail { get; set; }
    }
}
