﻿using System;
using System.Collections.Generic;
using System.Text;
using PhoneBook.Model.Core;

namespace PhoneBook.Report.Model
{
    public class ListReportsResponse: PaginatedResponse
    {
        public List<CommunicationReportModel> Reports { get; set; }
    }
}
