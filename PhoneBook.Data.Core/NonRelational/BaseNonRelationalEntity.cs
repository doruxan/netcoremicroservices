﻿using System;
using System.Collections.Generic;
using System.Text;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace PhoneBook.Data.Core
{
    public class BaseNonRelationalEntity : IEntity, INonRelationalEntity
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }
        public DateTimeOffset CreatedDate { get; set; }
        public DateTimeOffset? ModifiedDate { get; set; }
        public bool IsDeleted { get; set; }
        
        /// <summary>
        /// for ordering purposes
        /// </summary>
        public long UnixTimestamp { get; set; }
    }
}
