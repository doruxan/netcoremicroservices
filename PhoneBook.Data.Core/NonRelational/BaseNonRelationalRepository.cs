﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using MongoDB.Driver;

namespace PhoneBook.Data.Core.NonRelational
{
    public class BaseNonRelationalRepository<T> : IRepository<T> where T : BaseNonRelationalEntity
    {
        private readonly IMongoContext context;
        private IMongoCollection<T> dbSet;

        public BaseNonRelationalRepository(IMongoContext context)
        {
            this.context = context;

            dbSet = context.GetCollection<T>(typeof(T).Name);
        }

        public async Task AddAsync(T entity)
        {
            entity.CreatedDate = DateTimeOffset.UtcNow;
            entity.UnixTimestamp = DateTimeOffset.Now.ToUnixTimeSeconds();
            context.AddCommand(() => dbSet.InsertOneAsync(entity));
        }

        public async Task<T> GetByIdAsync(object id)
        {
            var data = await dbSet.FindAsync(x =>x.Id == id.ToString());
            return data.SingleOrDefault();
        }

        public async Task<IEnumerable<T>> GetAllAsync()
        {
            var all = await dbSet.FindAsync(Builders<T>.Filter.Empty);
            return all.ToList();
        }

        public async Task UpdateAsync(T entity)
        {
            entity.ModifiedDate = DateTimeOffset.UtcNow;
            context.AddCommand(() => dbSet.ReplaceOneAsync(x => x.Id == entity.Id.ToString(), entity));
        }

        public async Task DeleteAsync(T entity)
        {
            context.AddCommand(() => dbSet.DeleteOneAsync(x => x.Id == entity.Id.ToString()));
        }

        public IQueryable<T> GetQueryable()
        {
            return dbSet.AsQueryable();
        }

        public async Task DeleteAsync(object id)
        {
            context.AddCommand(() => dbSet.DeleteOneAsync(x => x.Id == id.ToString()));
        }
    }
}
