﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PhoneBook.Data.Core.NonRelational
{
    public class NonRelationalDbConfiguration
    {
        public string ConnectionString { get; set; }
        public string DatabaseName { get; set; }
    }
}
