﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace PhoneBook.Data.Core.NonRelational
{
    public class MongoUnitOfWork : IUnitOfWork
    {
        private readonly IMongoContext _context;

        public MongoUnitOfWork(IMongoContext context)
        {
            _context = context;
        }

        public async Task<int> CommitAsync()
        {
            return await _context.SaveChanges();
        }

        public void Dispose()
        {
            _context.Dispose();
        }
    }
}
