﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PhoneBook.Data.Core
{
    public interface INonRelationalEntity
    {
        public string Id { get; set; }
    }
}
