﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace PhoneBook.Data.Core.Relational.Mapping
{
    public class BaseEntityMap<TEntity> : IEntityTypeConfiguration<TEntity> where TEntity: BaseRelationalEntity
    {
        private bool baseInvoked;
        public virtual void Configure(EntityTypeBuilder<TEntity> builder)
        {
            if (!baseInvoked) throw new Exception("All the base configure methods should be invoked.");
            builder.HasKey(p => p.Id);
            builder.Property(p => p.Id).IsRequired();
            builder.Property(p => p.Id);
            builder.Property(p => p.CreatedDate).IsRequired();
            builder.Property(p => p.ModifiedDate);
            builder.Property(p => p.IsDeleted).IsRequired();
        }
    }
}
