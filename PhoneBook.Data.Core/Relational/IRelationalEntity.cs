﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PhoneBook.Data.Core
{
    public interface IRelationalEntity
    {
        public long Id { get; set; }
    }
}
