﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace PhoneBook.Data.Core.Relational
{
    public class BaseRelationalRepository<T> : IRepository<T> where T : BaseRelationalEntity
    {
        private readonly DbContext context;
        private DbSet<T> dbSet;

        public BaseRelationalRepository(DbContext context)
        {
            this.context = context;
            dbSet = context.Set<T>();
        }
        public async Task AddAsync(T entity)
        {
            await dbSet.AddAsync(entity);
        }

        public async Task DeleteAsync(T entity)
        {
            if (context.Entry(entity).State == EntityState.Detached)
            {
                dbSet.Attach(entity);
            }
            entity.IsDeleted = true;
        }

        public async Task DeleteAsync(object id)
        {
            T entityToDelete = await dbSet.FindAsync((long)id);
            await DeleteAsync(entityToDelete);
        }

        public async Task<IEnumerable<T>> GetAllAsync()
        {
           return await dbSet.ToListAsync();
        }

        public async Task<T> GetByIdAsync(object id)
        {
            return await dbSet.FirstOrDefaultAsync(x => x.Id == (long)id);
        }

        public IQueryable<T> GetQueryable()
        {
            return dbSet.AsQueryable();
        }

        public async Task UpdateAsync(T entity)
        {
            context.Entry(entity).State = EntityState.Modified;
        }
    }
}
