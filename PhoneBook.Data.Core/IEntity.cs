﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PhoneBook.Data.Core
{
    public interface IEntity
    {
        DateTimeOffset CreatedDate { get; set; }
        DateTimeOffset? ModifiedDate { get; set; }
        bool IsDeleted { get; set; }
    }
}
