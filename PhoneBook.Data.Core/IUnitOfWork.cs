﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace PhoneBook.Data.Core
{
    public interface IUnitOfWork : IDisposable
    {
        Task<int> CommitAsync();
    }
}
