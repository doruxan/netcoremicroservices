﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace PhoneBook.Communication.Data.Migrations
{
    public partial class ContactSurname : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Surname",
                table: "Contacts",
                type: "text",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Surname",
                table: "Contacts");
        }
    }
}
