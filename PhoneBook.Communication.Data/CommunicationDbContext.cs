﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using PhoneBook.Communication.Data.Entites;
using PhoneBook.Data.Core;

namespace PhoneBook.Communication.Data
{
    public class CommunicationDbContext: DbContext
    {
        public CommunicationDbContext(DbContextOptions options) : base(options)
        { }

        public DbSet<CommunicationDetailEntity> CommunicationDetails { get; set; }
        public DbSet<ContactEntity> Contacts { get; set; }

        public override async Task<int> SaveChangesAsync(CancellationToken cancellationToken = default)
        {
            EntityAudition();
            var result = await base.SaveChangesAsync(cancellationToken);
            return result;
        }

        private void EntityAudition()
        {
            foreach (var entry in ChangeTracker.Entries())
            {
                try
                {
                    var entity = entry.Entity as BaseRelationalEntity;
                    if (entity == null)
                    {
                        continue;
                    }

                    if (entry.State == EntityState.Added)
                    {
                        if (entity.CreatedDate == DateTimeOffset.MinValue)
                        {
                            entity.CreatedDate = DateTimeOffset.Now;
                        }
                    }

                    if (entry.State == EntityState.Deleted)
                    {
                        (entry.Entity as BaseRelationalEntity).IsDeleted = true;
                        entry.State = EntityState.Modified;
                    }

                    if (entry.State != EntityState.Modified)
                        continue;

                    entity.ModifiedDate = DateTimeOffset.Now;
                }
                catch (Exception)
                {

                    throw;
                }
            }
        }
    }

}
