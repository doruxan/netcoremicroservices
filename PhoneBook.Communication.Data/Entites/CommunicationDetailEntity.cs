﻿using System;
using System.Collections.Generic;
using System.Text;
using PhoneBook.Communication.Model;
using PhoneBook.Data.Core;
using PhoneBook.Shared.Model;

namespace PhoneBook.Communication.Data.Entites
{
    public class CommunicationDetailEntity : BaseRelationalEntity
    {
        public CommunicationType Type { get; set; }
        public string Value { get; set; }
        
        public long ContactId { get; set; }
        public ContactEntity Contact { get; set; }
    }
}
