﻿using System;
using System.Collections.Generic;
using System.Text;
using PhoneBook.Data.Core;

namespace PhoneBook.Communication.Data.Entites
{
    public class ContactEntity : BaseRelationalEntity
    {
        public string Name { get; set; }
        public string Surname { get; set; }
        public List<CommunicationDetailEntity> Communications { get; set; }
    }
}
