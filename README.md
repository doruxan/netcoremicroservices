## PhoneBook Service

**Two microservices with cqrs and event management**

** - Communication service**
Uses Postgres to store data with ef core. 
Publishes events to kafka when contact detail added.
So report service can have it's own data to create reports.
Uses a rest api to interact.

** - Report service**
Uses MongoDb to store data with ef core. 
Consumes from and publishes events to kafka.
Uses a rest api to interact.
Uses a hosted service to create reports.

**With an Api Gateway**
Microservices are reached from a Ocelot api gateway with a swagger configuration.
You can use gateway's swagger to test services.

**To Run**
- Services need Postgres, Mongo and Kafka to operate

- Mongo is served on Atlas because of easiness of clustering for testing purposes

For Postgres run below docker command:
docker run -e POSTGRES_USER=postgres -e POSTGRES_PASSWORD=password postgres -p 5432:5432

For Kafka, save below script to a yml file. Go to file destination and type command: docker-compose up



version: '2'
services:
  zookeeper:
    image: confluentinc/cp-zookeeper:latest
    environment:
      ZOOKEEPER_CLIENT_PORT: 2181
      ZOOKEEPER_TICK_TIME: 2000

  kafka:
    image: confluentinc/cp-kafka:latest
    depends_on:
      - zookeeper
    ports:
      - 9092:9092
    environment:
      KAFKA_BROKER_ID: 1
      KAFKA_ZOOKEEPER_CONNECT: zookeeper:2181
      KAFKA_ADVERTISED_LISTENERS: PLAINTEXT://kafka:29092,PLAINTEXT_HOST://localhost:9092
      KAFKA_LISTENER_SECURITY_PROTOCOL_MAP: PLAINTEXT:PLAINTEXT,PLAINTEXT_HOST:PLAINTEXT
      KAFKA_INTER_BROKER_LISTENER_NAME: PLAINTEXT
      KAFKA_OFFSETS_TOPIC_REPLICATION_FACTOR: 1
	  
	  
	  

**After Setting Up Dbs and Queue**
Go to data projects of api's and run "update-database" from package manager console or "dotnet ef database update" from cmd.


