﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Confluent.Kafka;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Newtonsoft.Json;
using PhoneBook.Business.Core;
using PhoneBook.Business.Core.Interfaces;

namespace PhoneBook.Business.Core
{
    public class QueueMessageListener : IQueueMessageListener, IHostedService
    {
        private readonly IEventProcessor eventProcessor;
        private readonly IConfiguration config;
        private readonly string topic;
        private readonly IConsumer<Ignore, string> kafkaConsumer;

        public QueueMessageListener(
                IEventProcessor eventProcessor,
                IConfiguration config
            )
        {
            this.eventProcessor = eventProcessor;
            this.config = config;
            //var consumerConfig = new ConsumerConfig();
            //config.GetSection("Kafka:ConsumerSettings").Bind(consumerConfig);
            var consumerConfig = new ConsumerConfig
            {
                BootstrapServers = "localhost:9092",
                GroupId = "foo",
                //AutoOffsetReset = AutoOffsetReset.Earliest
            };
            //this.topic = config.GetSection("Kafka")["Topic"];            
            this.topic = "demo";
            this.kafkaConsumer = new ConsumerBuilder<Ignore, string>(consumerConfig).Build();
        }
        public Task StartAsync(CancellationToken cancellationToken)
        {
            new Thread(async () => await Subscribe(cancellationToken)).Start();

            return Task.CompletedTask;
        }

        public async Task Subscribe(CancellationToken cancellationToken)
        {
            kafkaConsumer.Subscribe(this.topic);

            // from kafka examples
            while (!cancellationToken.IsCancellationRequested)
            {
                try
                {
                    var cr = this.kafkaConsumer.Consume(cancellationToken);
                    await MessageAction(cr.Message.Value);
                    Console.WriteLine($"{cr.Message.Key}: {cr.Message.Value}ms");
                }
                catch (OperationCanceledException)
                {
                    break;
                }
                catch (ConsumeException e)
                {
                    // Consumer errors should generally be ignored (or logged) unless fatal.
                    Console.WriteLine($"Consume error: {e.Error.Reason}");

                    if (e.Error.IsFatal)
                    {
                        // https://github.com/edenhill/librdkafka/blob/master/INTRODUCTION.md#fatal-consumer-errors
                        break;
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine($"Unexpected error: {e}");
                    break;
                }
            }
        }

        public async Task StopAsync(CancellationToken cancellationToken)
        {
            this.kafkaConsumer.Close(); // Commit offsets and leave the group cleanly.
            this.kafkaConsumer.Dispose();
        }


        public async Task MessageAction(string message)
        {
            try
            {
                var queueMessage = JsonConvert.DeserializeObject<QueueMessage>(message);

                if (string.IsNullOrEmpty(queueMessage.Type))
                    return;

                var type = GetType(queueMessage.Type);

                if (typeof(IEvent).IsAssignableFrom(type))
                {
                    var obj = JsonConvert.DeserializeObject(queueMessage.Payload, type);
                    var eventObject = obj as IEvent;


                    await eventProcessor.ProcessAsync(eventObject);
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public Type GetType(string typeName)
        {
            var type = Type.GetType(typeName);

            if (type != null)
                return type;
            var ssemblies = AppDomain.CurrentDomain.GetAssemblies();

            foreach (var a in AppDomain.CurrentDomain.GetAssemblies())
            {
                type = a.GetType(typeName);
                if (type != null)
                    return type;
            }
            return null;
        }

    }
}
