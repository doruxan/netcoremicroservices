﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using PhoneBook.Business.Core.Interfaces;
using Microsoft.Extensions.DependencyInjection;
using System.Threading.Tasks;

namespace PhoneBook.Business.Core
{
    public class CommandBus : ICommandBus
    {
        protected readonly IServiceProvider serviceProvider;

        public CommandBus(IServiceProvider serviceProvider)
        {
            this.serviceProvider = serviceProvider;
        }

        [DebuggerStepThrough]
        public void Send<K, T>(K command) where K : ICommand<T>
        {
            var handler = serviceProvider.GetService<ICommandHandler<K, T>>();

            if (handler == null)
                throw new Exception(string.Format("Command handler not found for command type {0}", nameof(command)));

            command.Result = true;

            try
            {
                handler.Handle(command);
            }
            catch (Exception)
            {
                command.Result = false;
                throw;
            }
        }

        [DebuggerStepThrough]
        public async Task SendAsync<K, T>(K command) where K : IAsyncCommand<T>
        {
            var handler = serviceProvider.GetService<ICommandHandler<K, T>>();

            if (handler == null)
                throw new Exception(string.Format("Command handler not found for command type {0}", nameof(command)));

            if (!(handler is IAsyncCommandHandler<K, T>))
                throw new Exception(string.Format("Command handler {0} does not support async", nameof(handler)));

            command.Result = true;

            try
            {
                await (handler as IAsyncCommandHandler<K, T>).HandleAsync(command).ConfigureAwait(false);
            }
            catch (Exception)
            {
                command.Result = false;
                throw;
            }
        }
    }
}
