﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Confluent.Kafka;
using PhoneBook.Business.Core.Interfaces;

namespace PhoneBook.Business.Core
{
    public class QueueMessageProducer<K, V>:IQueueMessageProducer<K,V>
    {
        IProducer<K, V> kafkaHandle;

        public QueueMessageProducer(IQueueClientHandle handle)
        {
            kafkaHandle = new DependentProducerBuilder<K, V>(handle.Handle).Build();
        }

        public Task ProduceAsync(string topic, Message<K, V> message)
            => this.kafkaHandle.ProduceAsync(topic, message);

        public void Produce(string topic, Message<K, V> message, Action<DeliveryReport<K, V>> deliveryHandler = null)
            => this.kafkaHandle.Produce(topic, message, deliveryHandler);

        public void Flush(TimeSpan timeout)
            => this.kafkaHandle.Flush(timeout);

        
    }
}
