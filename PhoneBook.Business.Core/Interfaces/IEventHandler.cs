﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace PhoneBook.Business.Core.Interfaces
{
    public interface IEventHandler<T> where T : IEvent
    {
        void Handle(T @event);
    }

    public interface IAsyncEventHandler<T> : IEventHandler<T> where T : IEvent
    {
        Task HandleAsync(T @event);
    }
}
