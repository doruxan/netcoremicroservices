﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PhoneBook.Business.Core.Interfaces
{
    public interface ICommand<T>
    {
        bool Result { get; set; }
        T ReturnValue { get; set; }
    }

    public interface IAsyncCommand<T> : ICommand<T> { }
}
