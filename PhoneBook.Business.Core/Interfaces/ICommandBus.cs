﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace PhoneBook.Business.Core.Interfaces
{
    public interface ICommandBus
    {
        void Send<K, T>(K command) where K : ICommand<T>;
        Task SendAsync<K, T>(K command) where K : IAsyncCommand<T>;
    }
}
