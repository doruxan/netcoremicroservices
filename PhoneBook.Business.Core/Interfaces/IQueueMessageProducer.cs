﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Confluent.Kafka;

namespace PhoneBook.Business.Core.Interfaces
{
    public interface IQueueMessageProducer<K,V>
    {
        Task ProduceAsync(string topic, Message<K, V> message);

        void Produce(string topic, Message<K, V> message, Action<DeliveryReport<K, V>> deliveryHandler = null);

        void Flush(TimeSpan timeout);
    }
}
