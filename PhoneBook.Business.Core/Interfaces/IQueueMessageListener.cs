﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace PhoneBook.Business.Core.Interfaces
{
    public interface IQueueMessageListener
    {
        Task Subscribe(CancellationToken cancellationToken);
    }
}
