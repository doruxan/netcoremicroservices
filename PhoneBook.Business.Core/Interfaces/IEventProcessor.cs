﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace PhoneBook.Business.Core.Interfaces
{
    public interface IEventProcessor
    {
        Task ProcessAsync<T>(T @event) where T : IEvent;
    }
}
