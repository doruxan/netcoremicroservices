﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace PhoneBook.Business.Core.Interfaces
{
    public interface IEventBus
    {
        Task PublishAsync(string topic, IEvent @event);
    }
}
