﻿using System;
using System.Collections.Generic;
using System.Text;
using Confluent.Kafka;

namespace PhoneBook.Business.Core.Interfaces
{
    public interface IQueueClientHandle
    {
        public Handle Handle { get; }
    }
}
