﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace PhoneBook.Business.Core.Interfaces
{
    public interface ICommandHandler<K, T> where K : ICommand<T>
    {
        void Handle(K command);
    }

    public interface IAsyncCommandHandler<K, T> : ICommandHandler<K, T> where K : ICommand<T>
    {
        Task HandleAsync(K command);
    }
}
