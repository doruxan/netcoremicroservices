﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;

namespace PhoneBook.Business.Core
{
    public class QueueMessage
    {
        public Guid Id { get; set; } = Guid.NewGuid();
        public string Type { get; set; }
        public string Payload { get; set; }

        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }
    }
}
