﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using PhoneBook.Business.Core.Interfaces;

namespace PhoneBook.Business.Core
{
    public class EventProcessor : IEventProcessor
    {
        private readonly IServiceProvider serviceProvider;

        public EventProcessor(IServiceProvider serviceProvider)
        {
            this.serviceProvider = serviceProvider;
        }

        public async Task ProcessAsync<T>(T @event) where T : IEvent
        {
            var handlerGeneric = typeof(IEventHandler<>);
            Type[] typeArgs = { @event.GetType() };
            var handlerType = handlerGeneric.MakeGenericType(typeArgs);

            var factory = serviceProvider.GetService<IServiceScopeFactory>();

            using (var scope = factory.CreateScope())
            {
                var handler = scope.ServiceProvider.GetService(handlerType);

                if (handler == null)
                    throw new Exception(string.Format("Event handler not found for event type {0}", nameof(@event)));

                await ((dynamic)handler).HandleAsync(@event as dynamic);
            }
        }
    }
}
