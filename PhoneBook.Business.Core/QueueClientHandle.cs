﻿using System;
using System.Collections.Generic;
using System.Text;
using Confluent.Kafka;
using Microsoft.Extensions.Configuration;
using PhoneBook.Business.Core.Interfaces;

namespace PhoneBook.Business.Core
{
    public class QueueClientHandle : IDisposable, IQueueClientHandle
    {
        IProducer<byte[], byte[]> kafkaProducer;

        public QueueClientHandle(IConfiguration config)
        {
            var conf = new ProducerConfig();
            conf.BootstrapServers = "localhost:9092";
            this.kafkaProducer = new ProducerBuilder<byte[], byte[]>(conf).Build();
        }

        public Handle Handle { get => this.kafkaProducer.Handle; }


        public void Dispose()
        {
            kafkaProducer.Flush();
            kafkaProducer.Dispose();
        }
    }
}
