﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Confluent.Kafka;
using Newtonsoft.Json;
using PhoneBook.Business.Core.Interfaces;

namespace PhoneBook.Business.Core
{
    public class EventBus : IEventBus
    {
        private readonly IQueueMessageProducer<string, string> queueMessageProducer;

        public EventBus(
            IQueueMessageProducer<string, string> queueMessageProducer)
        {
            this.queueMessageProducer = queueMessageProducer;
        }
        public async Task PublishAsync(string topic, IEvent @event)
        {
            var message = new QueueMessage
            {
                Type = @event.GetType().ToString(),
                Payload = JsonConvert.SerializeObject(@event)
            };
            await queueMessageProducer.ProduceAsync(topic, new Message<string, string> { Key = message.Id.ToString(), Value = message.ToString() });
        }
    }
}
