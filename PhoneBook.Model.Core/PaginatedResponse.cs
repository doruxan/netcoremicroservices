﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PhoneBook.Model.Core
{
    public class PaginatedResponse
    {
        public int Page { get; set; }
        public int PageSize { get; set; }
        public long TotalCount { get; set; }
        public int MaxPages { get; set; }
    }
}
