﻿using System;

namespace PhoneBook.Model.Core
{
    public class PaginatedRequest
    {
        public int Page { get; set; } = 1;
        public int PageSize { get; set; } = 20;

    }
}
