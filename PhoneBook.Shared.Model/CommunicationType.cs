﻿using System;
using System.Runtime.Serialization;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace PhoneBook.Shared.Model
{
    [JsonConverter(typeof(StringEnumConverter))]
    public enum CommunicationType
    {
        [EnumMember(Value = "Phone")]
        Phone,

        [EnumMember(Value = "Email")]
        Email,

        [EnumMember(Value = "Address")]
        Address
    }
}
