﻿using System;
using System.Collections.Generic;
using System.Text;
using PhoneBook.Report.Data.Mapping;

namespace PhoneBook.Report.Data
{
    public class DbBootstrapper
    {
        public static void ConfigureDb()
        {
            CustomerDetailsEntityMap.Configure();
            CommunicationReportDetailEntityMap.Configure();
            CommunicationReportEntityMap.Configure();
        }
    }
}
