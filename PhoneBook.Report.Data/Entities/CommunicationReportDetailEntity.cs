﻿using System;
using System.Collections.Generic;
using System.Text;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using PhoneBook.Data.Core;

namespace PhoneBook.Report.Data.Entities
{
    public class CommunicationReportDetailEntity : BaseNonRelationalEntity
    {
        public long ContactCount { get; set; }
        public long PhoneCount { get; set; }
        public string Location { get; set; }

        [BsonRepresentation(BsonType.ObjectId)]
        [BsonElement("reportId")]
        public string ReportId { get; set; }

        [BsonIgnore]
        public CommunicationReportEntity Report { get; set; }
    }
}
