﻿using System;
using System.Collections.Generic;
using System.Text;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using PhoneBook.Data.Core;
using PhoneBook.Report.Model;

namespace PhoneBook.Report.Data.Entities
{
    public class CommunicationReportEntity: BaseNonRelationalEntity
    {
        public DateTimeOffset RequestDate { get; set; }
        public ReportStatus Status { get; set; }
        public string Location { get; set; }


        [BsonRepresentation(BsonType.ObjectId)]
        [BsonElement("detailId")]
        public string DetailId { get; set; }

        [BsonIgnore]
        public CommunicationReportDetailEntity Detail { get; set; }

    }
}
