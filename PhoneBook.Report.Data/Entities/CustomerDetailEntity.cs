﻿using System;
using System.Collections.Generic;
using System.Text;
using PhoneBook.Data.Core;

namespace PhoneBook.Report.Data.Entities
{
    public class CustomerDetailEntity: BaseNonRelationalEntity
    {
        public long CustomerId { get; set; }
        public string Location { get; set; }
        public int PhoneCount { get; set; }
    }
}
