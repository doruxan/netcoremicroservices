﻿using System;
using System.Collections.Generic;
using System.Text;
using MongoDB.Bson.Serialization;
using PhoneBook.Report.Data.Entities;

namespace PhoneBook.Report.Data.Mapping
{
    public class CommunicationReportEntityMap
    {
        public static void Configure()
        {
            BsonClassMap.RegisterClassMap<CommunicationReportEntity>(map =>
            {
                map.AutoMap();
                map.SetIgnoreExtraElements(true);
                map.MapIdMember(x => x.Id);
            });
        }
    }
}
